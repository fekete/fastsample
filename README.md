# fastsample

Fast sampling over arrays

Use ideas from Eriker Landson http://erikerlandson.github.io/blog/2014/09/11/faster-random-samples-with-gap-sampling/ and Daniel Lemire https://lemire.me/blog/2019/05/07/almost-picking-n-distinct-numbers-at-random/

Code for wyhash is in https://github.com/wangyi-fudan/wyhash

Code from Daniel Lemire in https://github.com/lemire/Code-used-on-Daniel-Lemire-s-blog/tree/master/2019/05/07  

Results are, in order using qsort, using std::sort, using Lemire `fast_pick`, and using Landson `faster_pick`:

```
Generating 1000000 values in [0, 40000000000), density: 0.002500 %
timings: 0.140035 s 0.071724 s 0.059264 s 0.043044 s 
timings per value: 140.035000 ns 71.724000 ns 59.264000 ns 43.044000 ns 
actual counts: 999993 999985 999998 1000000

Generating 10000000 values in [0, 40000000000), density: 0.025000 %
timings: 1.631888 s 0.831434 s 0.595500 s 0.422797 s 
timings per value: 163.188800 ns 83.143400 ns 59.550000 ns 42.279700 ns 
actual counts: 9998731 9998733 9999998 10000000

Generating 100000000 values in [0, 40000000000), density: 0.250000 %
timings: 18.462078 s 9.527892 s 5.930176 s 4.220071 s 
timings per value: 184.620780 ns 95.278920 ns 59.301760 ns 42.200710 ns 
actual counts: 99875567 99874864 100000000 100000000
```
